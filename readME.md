# ![](img/se_logo.png) PictsManager mobile app


### Steps to build the app:

- install Android SDK for Android platform and XCode for iOS
- make sure you installed [Node.js](http://nodejs.org/)
- install npm `$ npm install`
- run the following command in a terminal
```bash
$ npm start
$ i #for ios
$ a #for andrïde
```
### For development:
- open a terminal and run the following command:
```bash
$ cd front 
$ code . # open in VS code <android | ios>
```

On iOS you may need to grant permissions to `.npm` and `.config` before running the build or dev script
```bash
$ sudo chown -R $USER:$GROUP ~/.npm
$ sudo chown -R $USER:$GROUP ~/.config