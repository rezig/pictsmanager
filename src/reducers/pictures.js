import { 
  ADD_PICTURE,
  ADD_PICTURE_ALBUM,
  GET_PICTURES, 
  GET_PICTURE_DETAILS,
  GET_ALBUM_PICTURES,
  DELETE_PICTURE,
} from '../actions/types.js'

const initialState = {
  pictures: [],
  albumpictures: [],
  picture: null,
};

export default function (state= initialState, action) {
  switch (action.type) {
    case ADD_PICTURE:
      return {
        ...state,
        pictures: [...state.pictures, action.payload],
      }
    case ADD_PICTURE_ALBUM:
      return {
        ...state,
        pictures: action.payload,
      }
    case GET_PICTURES:
      return {
        ...state,
        pictures: action.payload,
        picturesAccess: [],
      }
    case GET_PICTURE_DETAILS:
      return {
        ...state,
        picture: action.payload,
      }
    case GET_ALBUM_PICTURES:
      return {
        ...state,
        albumpictures: action.payload,
      }
    case DELETE_PICTURE:
      return {
        ...state, 
        pictures: state.pictures.filter((picture) => picture.id !== action.payload),
      }
    default:
      return state
  }
}