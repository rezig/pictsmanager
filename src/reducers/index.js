import { combineReducers } from 'redux';
import auth from './auth'
import pictures from './pictures'
import albums from './albums'
import albumsAccess from './albumsAccess'
import picturesAccess from './picturesAccess'

export default combineReducers({
    auth,
    pictures,
    albums,
    albumsAccess,
    picturesAccess,
});
  