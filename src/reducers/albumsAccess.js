import { 
  ADD_ALBUM_ACCESS,
  GET_ALBUMS_ACCESS,
  DELETE_ALBUM_ACCESS,
} from '../actions/types.js'


const initialState = {
  albumsAccess: [],
  album: null,
};

export default function (state= initialState, action) {
  switch (action.type) {
    case ADD_ALBUM_ACCESS:
      return {
        ...state,
        albumsAccess: [...state.albumsAccess, action.payload],
      }
    case GET_ALBUMS_ACCESS:
      return {
        ...state,
        albumsAccess: action.payload,
      }
    case DELETE_ALBUM_ACCESS:
      return {
        ...state,
        albumsAccess: action.payload,
    }
    default:
      return state
  }
}