import { ADD_ALBUM,
  GET_ALBUMS,
  DELETE_ALBUM,
} from '../actions/types.js'


const initialState = {
  albums: [],
  album: null,
};

export default function (state= initialState, action) {
  switch (action.type) {
    case ADD_ALBUM:
      return {
        ...state,
        album:  [...state.albums, action.payload],
      }
    case GET_ALBUMS:
      return {
        ...state,
        albums: action.payload,
    }
    case DELETE_ALBUM:
      return {
        ...state,
        albums: state.albums.filter((album) => album.id !== action.payload),
      }
    default:
      return state
  }
}