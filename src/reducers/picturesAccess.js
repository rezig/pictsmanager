import { 
  ADD_PICTURE_ACCESS,
  GET_PICTURES_ACCESS,
  GET_PICTURE_DETAILS,
  GET_ALBUM_PICTURES,
  DELETE_PICTURE_ACCESS,
} from '../actions/types.js'

const initialState = {
  picturesAccess: [],
  picture: null,
};

export default function (state= initialState, action) {
  switch (action.type) {
    case ADD_PICTURE_ACCESS:
      return {
        ...state,
        pictures: [...state.picturesAccess, action.payload],
      }
    case GET_PICTURES_ACCESS:
      return {
        ...state,
        pictures: [],
        picturesAccess: action.payload,
      }
    case GET_PICTURE_DETAILS:
      return {
        ...state,
        picture: action.payload,
      }
    case GET_ALBUM_PICTURES:
      return {
        ...state,
        picture: action.payload,
      }
    case DELETE_PICTURE_ACCESS:
      return {
        ...state,
        picture: action.payload,
      }
    default:
      return state
  }
}