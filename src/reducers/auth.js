import { LOGIN_SUCCESS, LOGIN_FAIL, REGISTER_SUCCESS, REGISTER_FAIL } from '../actions/types'
import AsyncStorage from '@react-native-community/async-storage';

const initialState = {
  token: AsyncStorage.getItem('access_token'),
  user: null,
  isLoading: false,
  isAuthenticated: false,
}

export default function (state= initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      AsyncStorage.setItem('access_token', action.payload.access_token)
      return {
        ...state,
        ...action.payload,
        user: action.payload.user,
        isAuthenticated: true,
        isLoading: false,
      }
    case REGISTER_SUCCESS:
      return {
        ...action.payload
      }
    case LOGIN_FAIL:
    case REGISTER_FAIL:
      return {
        ...state,
        token: null,
        user: null,
        isAuthenticated: false,
        isLoading: false,
      }
    default:
      return state
  }
}