import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { connect } from 'react-redux';
import { MaterialCommunityIcons } from '@expo/vector-icons'

import SignInScreen from './components/signin'
import SignUpScreen from './components/signup'
import HomeScreen from './components/home'
import CameraScreen from './components/camera'
import ProfileScreen from './components/profile'
import DetailPictureScreen from './components/detailPicture'
import DetailPictureAccessScreen from './components/detailPictureAccess'
import DetailAlbumScreen from './components/detailAlbum'
import DetailAlbumAccessScreen from './components/detailAlbumAccess'
import SavePictureScreen from './components/savePicture'

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function ProfileStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="My Profile" component={ProfileScreen} />
      <Stack.Screen name="DetailPictureScreen" component={DetailPictureScreen} />
      <Stack.Screen name="DetailAlbumScreen" component={DetailAlbumScreen} />
    </Stack.Navigator>
  );
}

function CameraStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="CameraScreen" component={CameraScreen} />
      <Stack.Screen name="SavePictureScreen" component={SavePictureScreen} />
    </Stack.Navigator>
  );
}

function HomeStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen 
        name="Home"
        component={HomeScreen} 
        options={{ 
          title: 'Home',
        }}
      />
      <Stack.Screen 
        name="DetailPictureAccessScreen" 
        component={DetailPictureAccessScreen}
        options={{ 
          title: 'Detail Picture',
        }}
      />
      <Stack.Screen 
        name="DetailAlbumAccessScreen"
        component={DetailAlbumAccessScreen}
        options={{ 
          title: 'Detail Album',
        }}
      />
    </Stack.Navigator>
  );
}

function Routing(auth) {

	return (
    
    <NavigationContainer>
      { !auth.auth.isAuthenticated ? (
        <Stack.Navigator>
          <Stack.Screen 
            name="SignInScreen" 
            component={SignInScreen}
            options={{ 
              title: 'Sign In',
              headerStyle: {
                backgroundColor: '#005076',
              },
            }}
          />
          <Stack.Screen 
            name="SignUpScreen" 
            component={SignUpScreen} 
            options={{ 
              title: 'Sign Up',
              headerStyle: {
                backgroundColor: '#005076',
              },
            }}
          />
        </Stack.Navigator>
      ) : (
        <Tab.Navigator
          tabBarOptions={{
            activeTintColor: '#e91e63',
          }}
        >
          <Tab.Screen
            name="HomeScreen" 
            component={HomeStack}
            options={{
              tabBarLabel: 'Home',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="home" color={color} size={size} />
              ),
            }}
          />
          <Tab.Screen 
            name="CameraScreen"
            component={CameraStack} 
            options={{
              tabBarLabel: 'camera',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="camera" color={color} size={size} />
              ),
            }}
          />
          <Tab.Screen 
            name="ProfileScreen"
            component={ProfileStack} 
            options={{
              tabBarLabel: 'profile',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="account" color={color} size={size} />
              ),
            }}
          />
        </Tab.Navigator>
      )}
			</NavigationContainer>
	);
}
  
const mapStateToProps = (state) => ({
  auth: state.auth
});

export default connect(mapStateToProps)(Routing);
