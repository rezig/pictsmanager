import axios from 'axios'

import { LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT_SUCCESS, REGISTER_SUCCESS , REGISTER_FAIL } from './types';


// LOGIN USER
export const login = (username, password) => (dispatch) => {
    // Headers
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    // Request Body  
    const body = JSON.stringify({"user": { username, password }});
  
    axios
      .post('http://localhost:3000/users/signin', body, config)
      .then((res) => {
        dispatch({
          type: LOGIN_SUCCESS,
          payload: res.data,
        });
      })
      .catch((err) => {
        console.warn(err)
        dispatch({
          type: LOGIN_FAIL,
        });
      });
};



// REGISTER USER
export const register = (username, password) => (dispatch) => {
    // Headers
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    // Request Body  
    const body = JSON.stringify({"user": { username, password }});
  
    axios
      .post('http://localhost:3000/users/signup', body, config)
      .then((res) => {
        dispatch({
          type: REGISTER_SUCCESS,
          payload: res.data,
        });
      })
      .catch((err) => {
        console.warn(err)
        dispatch({
          type: REGISTER_FAIL,
        });
      });
};
  
