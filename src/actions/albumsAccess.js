import axios from 'axios'
import { 
  ADD_ALBUM_ACCESS,
  GET_ALBUMS_ACCESS,
  DELETE_ALBUM_ACCESS,
} from './types'


// ADD ACCESS TO ALBUM                                              
export const addAlbumAccess = (userName, albumID) => (dispatch) => {

  axios
    .post(`http://localhost:3000/albums/${albumID}/access/users/username/${userName}`)
    .then((res) => {
      dispatch({
        type: ADD_ALBUM_ACCESS,
        payload: res.data,
      })
    })
}

//GET_ALBUMS_ACCESS
export const getAlbumsAccess = (user) => (dispatch) => {
  // Headers
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  }
  
  axios
    .get(`http://localhost:3000/albums/access/users/${user}`)
    .then((res) => {
      dispatch({
        type: GET_ALBUMS_ACCESS,
        payload: res.data,
      })
    })
} 

//DELETE_ACCESS_ALBUM
export const deleteAlbumAccess = (pictureId, user) => (dispatch) => {
  axios
    .delete(`http://localhost:3000/albums/${pictureId}/access/users/username/${user}`)
    .then((res) => {
      dispatch({
        type: DELETE_ALBUM_ACCESS,
        payload: res.data,
      })
    })
}
