import axios from 'axios'
import { ADD_PICTURE,
  ADD_PICTURE_ALBUM,
  GET_PICTURES, 
  GET_PICTURE_DETAILS,
  GET_ALBUM_PICTURES,
  DELETE_PICTURE,
} from './types'
import { store } from '../store'

// ADD A NEW PICTURE                                                                 
export const addPicture = (user, pictureUri) => (dispatch) => {
  // Headers
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  }
  console.log(pictureUri)
  
  // Request Body  
  var formData = new FormData()
  formData.append('fileNow', {
    link: pictureUri,
    uri: pictureUri,
    name: 'besma.jpg',
    type: 'image/jpg',
  })
  
  const userId = user.id
 
  axios
    .post(`http://localhost:3000/pictures/upload/users/${userId}`,formData, config.headers )
    .then((res) => {
      dispatch({
        type: ADD_PICTURE,
        payload: res.data,
      }, getPictures())
    })

}

// ADD A NEW PICTURE TO AN ALBUM                    
export const addPictureAlbum = (albumId, userId, pictureUri) => (dispatch) => {
  const id = userId.id
  // Headers
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  }
  
  // Request Body  
  var formData = new FormData()
  formData.append('fileNow', {
    uri: pictureUri,
    name: 'besma.jpg',
    type: 'image/jpg',
  })
  console.log('uril '+pictureUri)

  axios
    .post(`http://localhost:3000/pictures/upload/users/${id}/albums/${albumId}`,
      formData, config.headers)
    .then((res) => {
      dispatch({
        type: ADD_PICTURE_ALBUM,
        payload: res.data,
      })
    })
}

// GET PICTURES for user (owner)                     DONE
export const getPictures = () => (dispatch) => {
  const userId = store.getState().auth.user.id
  axios
    .get(`http://localhost:3000/pictures/users/${userId}`)
    .then((res) => {
      dispatch({
        type: GET_PICTURES,
        payload: res.data,
      })
    })
    .catch((err) => {console.warn(err)})
}


//GET_PICTURE_DETAILS                                                            
export const getPictureDetails = (id) => (dispatch) => {
  
  axios
    .get(`http://localhost:3000/pictures/2`)
    .then((res) => {
      dispatch({
        type: GET_PICTURE_DETAILS,
        payload: res.data,
      })
    })
}

// GET_ALBUM_PICTURES
export const getAlbumPictures = (albumid) => (dispatch) => {
  
  axios
    .get(`http://localhost:3000/pictures/albums/${albumid}`)
    .then((res) => {
      dispatch({
        type: GET_ALBUM_PICTURES,
        payload: res.data,
      })
    })
}

//DELETE_PICTURE                                           
export const deletePicture = (pictureId) => (dispatch) => {
  axios
    .delete(`http://localhost:3000/pictures/${pictureId}`)
    .then((res) => {
      dispatch(getPictures())
    })
    .catch((err) => console.log(err));
}