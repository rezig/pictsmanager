import axios from 'axios'
import { ADD_ALBUM,
  GET_ALBUMS,
  DELETE_ALBUM,
} from './types'
import { store } from '../store'

// ADD A NEW ALBUM                              
export const addAlbum = (user, albumName) => (dispatch) => {
  // Headers
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  }
  // Request Body  
  var name = albumName
  const body = JSON.stringify({ name })

  axios
    .post(`http://localhost:3000/albums/users/${user}`, body, config)
    .then((res) => {
      dispatch({
        type: ADD_ALBUM,
        payload: res.data,
      })
      console.log('done')
      dispatch(getAlbums())
    })
}

// GET ALL ALBUMS OF AN USER                    
export const getAlbums = () => (dispatch) => {
  const userId = store.getState().auth.user.id
  axios
    .get(`http://localhost:3000/albums/users/${userId}`)
    .then((res) => {
      dispatch({
        type: GET_ALBUMS,
        payload: res.data,
      });
    })
    .catch((err) => {console.warn(err)})
};

//DELETE_ALBUM                                       
export const deleteAlbum = (albumId) => (dispatch) => {
  axios
    .delete(`http://localhost:3000/albums/${albumId}`)
    .then((res) => {
      dispatch(getAlbums())
    })
    .catch((err) => console.log(err))
}
