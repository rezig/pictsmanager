import axios from 'axios'
import { 
  ADD_PICTURE_ACCESS,
  GET_PICTURES_ACCESS,
  DELETE_PICTURE_ACCESS,
} from './types'


// ADD ACCESS TO USER TO PICTURE                          DONE             
export const addPictureAccess = (userId, pictureId) => (dispatch) => {
  axios
    .post(`http://localhost:3000/pictures/${pictureId}/access/users/${userId}`)
    .then((res) => {
      dispatch({
        type: ADD_PICTURE_ACCESS,
        payload: res.data,
      });
    })
    .catch((err) => console.warn(err))
};

// GET PICTURES for user that he has access to            DONE
export const getPicturesAccess = (user) => (dispatch) => {
  
  axios
    .get(`http://localhost:3000/pictures/access/users/${user}`)
    .then((res) => {
      dispatch({
        type: GET_PICTURES_ACCESS,
        payload: res.data.slice(0, 6),
      });
    })
};

//DELETE_ACCESS_PICTURE,                                  
export const deletePictureAccess = (pictureId, user) => (dispatch) => {
  
  axios
    .delete(`http://localhost:3000/pictures/${pictureId}/access/users/username/${user}`)
    .then((res) => {
      dispatch({
        type: DELETE_PICTURE_ACCESS,
        payload: res.data,
      });
    })
};
