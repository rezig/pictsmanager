import React, { Component } from 'react'
import { View, Text, TouchableHighlight, Image, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { getAlbumPictures } from '../actions/pictures'

export class DetailAlbumeAccessScreen extends Component {
  static propTypes = {
    getAlbumPictures: PropTypes.func.isRequired,
  }
  
  componentDidMount() {
    this.props.getAlbumPictures(this.props.route.params.albumid)
  }

  render() {
    const { albumid, albumName } = this.props.route.params

    return (
      <View style={styles.container} >
        <Text style={styles.welcome} >Album Name : {albumName}</Text>
        { this.props.pictures.albumpictures.length != 0 ? 
        (
          <Text> {this.props.pictures.albumpictures.map((obj, index) => (
            <View key={index} >
                <View>
                  <TouchableHighlight
                    style={styles.touchable}
                    onPress={() => 
                      this.props.navigation.navigate('DetailPictureAccessScreen',
                        { 
                          pictureId: obj.id,
                          pictureName: obj.name,
                          pictureLink: obj.link,
                          pictureDate: obj.date,
                          pictureAlbum: obj.albumid,
                        }
                      )
                    }
                  >
                    <Image
                      style={styles.fullImageStyle}
                      source={{
                        uri: obj.link
                      }}
                    />
                  </TouchableHighlight>
                </View>
            </View>
            )
          )}
          </Text>
        ) : ( <Text style={styles.titleAlbum} > this Album is Empty</Text> )

        }
        
        <Text style={styles.titleAlbum}>{this.props.pictures.albumpictures.map((obj) => (obj.name) ) }</Text>
      </View>
    )
  }
}

const mapStateToProps = (state) => ({
  pictures: state.pictures,
})


export default connect((mapStateToProps), { getAlbumPictures })(DetailAlbumeAccessScreen)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
    marginTop: 30,
  },
  fullImageStyle:{
    width: 150,
    height: 120,
    marginBottom: 20,
  },
  welcome: {
    marginLeft: 5,
    marginTop: 20,
    marginBottom: 20,
    fontSize: 20,
  },
  button: {
    width: 160,
    flexDirection: 'row',
    backgroundColor: "#CCCCCC",
    borderRadius: 12,
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 0,
    marginBottom: 20
  },
  titleAlbum: {
    fontSize: 18,
    marginBottom: 10,
    marginLeft: 23,
  },
  touchable: {
    width:150, 
    height:120
  }
})