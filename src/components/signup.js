import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, TextInput } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { showMessage } from "react-native-flash-message"

import { register } from '../actions/auth'


class SignUpScreen extends Component {
    
  state = {
    username: "",
    password: "",
    password2: "",
  }

  static propTypes = {
    register: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool,
  };

  onSubmit = (e) => {
    const { username, password, password2 } = this.state
    e.preventDefault()
    if (username == "" || password == "" || password2 == "") {
      console.warn('required fiels')
    }
    else {
      if (password != password2) {
        console.warn('password doesnt match ')
      }
      else{
        this.props.register(username, password);
        showMessage({
          message: "New Album was created",
          description: 'A new album was successfully created',
          type: "success",
        })
      }
    }
  }

  onChangeHandle(state, value) {
    this.setState({
      [state]: value 
    })
  } 

  render(){
    const { username, password, password2 } = this.state

    return(
      <View style={styles.container}>
        <Text style={styles.logo}>PictsManager</Text>
        <View style={styles.inputView} >
            <TextInput  
              style={styles.inputText}
              name="username"
              placeholder="Email..." 
              placeholderTextColor="#003f5c"
              value={username}
              onChangeText={(value) => this.onChangeHandle('username', value)}/>
          </View>

          <View style={styles.inputView} >
            <TextInput  
              style={styles.inputText}
              name="password"
              secureTextEntry={true} 
              placeholder="Password..." 
              placeholderTextColor="#003f5c"
              value={password}
              onChangeText={(value) => this.onChangeHandle('password', value)}/>
          </View>

          <View style={styles.inputView} >
            <TextInput  
              style={styles.inputText}
              secureTextEntry={true} 
              name="password2"
              placeholder="Confirm Password..." 
              placeholderTextColor="#003f5c"
              value={password2}
              onChangeText={(value) => this.onChangeHandle('password2', value)}/>
          </View>

          <TouchableOpacity
            style={styles.loginBtn}
            title="Sign In" 
            onPress={(e) => this.onSubmit(e) }
          >
            <Text style={styles.loginText} >Sign In</Text>
          </TouchableOpacity>
          
      </View>
    )
  }
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});


export default connect(mapStateToProps, { register })(SignUpScreen)



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#003f5c',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    fontWeight: "bold",
    fontSize: 45,
    color: "#fb5b5a",
    marginBottom: 40
  },
  inputView: {
    width: "80%",
    backgroundColor: "#465881",
    borderRadius: 25,
    height: 50,
    marginBottom: 20,
    justifyContent: "center",
    padding: 20
  },
  inputText: {
    height: 50,
    color: "white"
  },
  loginBtn: {
    width: "80%",
    backgroundColor: "#fb5b5a",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10
  },
  loginText: {
    color: "white"
  }
});
