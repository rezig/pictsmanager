import React, { Component } from 'react'
import { View, Image, Text, StyleSheet, TouchableOpacity, Alert } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'


export class detailPictureAccessScreen extends Component {

  createPromptAlert = () =>
    Alert.prompt(
      "Picture Name",
      "Enter a Name for you picture",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => console.log('ok') //this.props.addPictureAccess(userId, pictureId)
        }
      ],
    );
  
  render() {
    const {pictureId, pictureName, pictureLink, pictureDate, pictureAlbum} = this.props.route.params

    return (
      <View style={styles.container}>
        <Image
          style={styles.fullImageStyle}
          source={{
            uri: pictureLink,
          }}
        />
        <Text style={styles.pictureName} > {pictureName} </Text>
        <Text>{ pictureAlbum !== null ?
                <Text>This picture belong to this album {JSON.stringify(pictureLink)}</Text> 
                :
                (<Text>This picture doesn't belong to any album</Text>) 
              }
        </Text>
        <TouchableOpacity
          title='save this picture'
          onPress={() => this.createPromptAlert() }
          style={styles.button}
        >
          <Text style={styles.text}>Save picture</Text>
        </TouchableOpacity> 
      </View>
    )
  }
}

export default detailPictureAccessScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    margin: 10
  },
  image: {
    flex: 1,
  },
  fullImageStyle:{
    width: 320,
    height: 310,
    marginBottom: 10,
    marginLeft: 20,
    marginTop:23,
  },
  pictureName: {
    textAlign: "center",
    fontSize: 23,
    color: 'red'
  },
  button:{
    width: "50%",
    backgroundColor: "#fb5b5a",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10,
    marginLeft: 92,
  },
  text:{
    marginLeft: 5,
    fontSize: 22,
    color: 'white'
  }
})