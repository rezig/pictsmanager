import React, { useState } from 'react';
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { useDispatch } from 'react-redux';

import { login } from '../actions/auth'

export default function SignInScreen({navigation}) {

  const [info, setInfo] = useState({username: 'Besma', password: '12345'})
  const dispatch = useDispatch()

  function onSubmit(e) {
    dispatch(login(info.username, info.password))
  }

  onChangeHandle = (key, value) => {
    info[key] = value
    setInfo({...info})
  }
  return(
    <View style={styles.container}>
      <Text style={styles.logo}>PictsManager</Text>
        <View style={styles.inputView} >
          <TextInput  
            style={styles.inputText}
            name="username"
            placeholder="Username..." 
            placeholderTextColor="#003f5c"
            value={info.username || ""}
            onChangeText={(value) => onChangeHandle('username', value)}/>
        </View>

        <View style={styles.inputView} >
          <TextInput  
            style={styles.inputText}
            name="password"
            placeholder="Password..." 
            placeholderTextColor="#003f5c"
            value={info.password}
            secureTextEntry={true} 
            onChangeText={(value) => onChangeHandle('password', value)}/>
        </View>

        <TouchableOpacity
          style={styles.loginBtn}
          title="Login" 
          onPress={(e) => onSubmit(e) }
        >
          <Text style={styles.loginText} >Sign In</Text>
        </TouchableOpacity>
        
        <TouchableOpacity
          title="Register" 
          onPress={(e) => navigation.navigate('SignUpScreen')}
          >
          <Text style={styles.loginText} >Sign Up</Text>
        </TouchableOpacity>
    </View>
  )
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#003f5c',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    fontWeight: "bold",
    fontSize: 45,
    color: "#fb5b5a",
    marginBottom: 40
  },
  inputView: {
    width: "80%",
    backgroundColor: "#465881",
    borderRadius: 25,
    height: 50,
    marginBottom: 20,
    justifyContent: "center",
    padding: 20
  },
  inputText: {
    height: 50,
    color: "white"
  },
  loginBtn: {
    width: "80%",
    backgroundColor: "#fb5b5a",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10
  },
  loginText: {
    color: "white"
  }
});
