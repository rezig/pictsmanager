import React, { Component } from 'react'
import { View, Image, Text, StyleSheet, ScrollView, Alert, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import FlashMessage from "react-native-flash-message"
import { showMessage } from "react-native-flash-message"

import { deletePicture } from '../actions/pictures'
import { addPictureAccess } from '../actions/picturesAccess'
import { deletePictureAccess } from '../actions/picturesAccess'

export class detailPictureScreen extends Component {
   
  static propTypes = {
    deletePicture: PropTypes.func.isRequired,
    addPictureAccess: PropTypes.func.isRequired,
    deletePictureAccess: PropTypes.func.isRequired,
  }

  createTwoButtonAlert = (pictureId) =>
    Alert.alert(
      "Are You Sure",
      "Deleting this picture will delete all the access to it",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", 
        onPress: () => {
          this.props.deletePicture(pictureId)
          this.props.navigation.goBack() 
        }
        },
      ],
      { cancelable: false }
    );

    createPromptAlert = (pictureId) =>
    Alert.prompt(
      "Enter user id",
      "Enter the id of the user that you want him to get access to your picture ",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: (userId) => {
            this.props.addPictureAccess(userId, pictureId);
            showMessage({
              message: "Access user was added",
              description: "A new user access was successfully added",
              type: "success",
            })
          }
        }
      ],
    );
    createAccessdelete = (pictureId) =>
    Alert.prompt(
      "Enter user Name",
      "Enter the Name of the user that you want to delete his access to this picture ",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: (user) => {
            this.props.deletePictureAccess(pictureId, user); 
            showMessage({
              message: "Access user was deleted",
              description: "The user access was successfully deleted",
              type: "success",
            })
          }
        }
      ],
    );

  render() {
    const {pictureId, pictureName, pictureLink, pictureDate, pictureAlbum} = this.props.route.params
                        
    return (
      <ScrollView style={styles.container}>
      <FlashMessage position="top" />

        <Text style={styles.pictureName}> {pictureName} </Text>
        <Image
          style={styles.fullImageStyle}
          source={{
            uri: pictureLink,
          }}
        />
        <Text>{ pictureAlbum != null ?
                <Text>This picture belong to this album {JSON.stringify(pictureLink)}</Text> 
                :
                (<Text>This picture doesn't belong to any album</Text>) 
              }
        </Text>
        <View style={styles.myButton}>
          <TouchableOpacity
            title='Add Access to Picture'
            onPress={() => this.createPromptAlert(pictureId) }
            style={styles.buttonSave}
          >
            <Text style={styles.text}>Add access</Text>
          </TouchableOpacity>

          <TouchableOpacity
            title='Delete Picture'
            onPress={() => this.createTwoButtonAlert(pictureId) }
            style={styles.buttonDelete}
          >
            <Text style={styles.text}>Delete Picture</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          title='Delete Access Picture'
          onPress={() => this.createAccessdelete(pictureId) }
          style={styles.btn}
        >
          <Text style={styles.text}>Delete Access Picture</Text>
        </TouchableOpacity>
        
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => ({
})


export default connect( mapStateToProps , { deletePicture, addPictureAccess, deletePictureAccess } )(detailPictureScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    margin: 10
  },
  btn: {
    width: "40%",
    backgroundColor: "#fb5b5a",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 100,
  },
  pictureName: {
    textAlign: "center",
    fontSize: 23,
    color: 'red'
  },
  fullImageStyle:{
    width: 320,
    height: 310,
    marginBottom: 20,
    marginLeft: 20,
    marginTop: 10,
  },
  buttonDelete:{
    width: "45%",
    backgroundColor: "#fb5b5a",
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10,
  },
  buttonSave:{
    width: "40%",
    backgroundColor: "green",
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10,
  },
  text:{
    marginLeft: 5,
    fontSize: 22,
    color: 'white'
  },
  myButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
})