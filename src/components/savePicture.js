import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, Alert, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { addPicture, addPictureAlbum } from '../actions/pictures'

export class savePictureScreen extends Component {
  state= {
    user: this.props.user.id
  }

  createPromptAlert = () =>
    Alert.prompt(
      "Enter album ID",
      "Enter the album ID that you want to save the picture in ",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: (albumID) => this.props.addPictureAlbum(albumID, this.props.user, this.props.route.params.pictureUri)
        }
      ],
    );
  
  static propTypes = {
    addPicture: PropTypes.func.isRequired,
    addPictureAlbum: PropTypes.func.isRequired,
  }
  render() {
    const {pictureUri} = this.props.route.params
    return (
      <View style={styles.container}>
        <Image
          style={styles.fullImageStyle}
          source={{
            uri: pictureUri,
          }}
        />
        <View style={styles.myButton}>
          <TouchableOpacity
            title='save in album'
            onPress={() => this.createPromptAlert() }
            style={styles.buttonSave}
          >
            <Text style={styles.text}>Save in Album</Text>
          </TouchableOpacity>
          <TouchableOpacity
            title='save this picture'
            onPress={() => this.props.addPicture(this.props.user, pictureUri)}
            style={styles.buttonDelete}
          >
            <Text style={styles.text}>Save Picture</Text>
          </TouchableOpacity>

        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.auth.user,
})

export default connect(mapStateToProps, {addPicture, addPictureAlbum})(savePictureScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    margin: 10
  },
  button:{
    width: "50%",
    backgroundColor: "#fb5b5a",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10,
    marginLeft: 92,
  },
  fullImageStyle:{
    width: 320,
    height: 310,
    marginBottom: 20,
    marginLeft: 20,
    marginTop:23,
  },
  text:{
    marginLeft: 5,
    fontSize: 22,
  },
  buttonDelete:{
    width: "45%",
    backgroundColor: "#fb5b5a",
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10,
  },
  buttonSave:{
    width: "40%",
    backgroundColor: "green",
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10,
  },
  text:{
    marginLeft: 5,
    fontSize: 22,
    color: 'white'
  },
  myButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
})