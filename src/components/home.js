import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, ScrollView, Button, TouchableHighlight, TextInput } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { getPicturesAccess } from '../actions/picturesAccess'
import { getAlbumsAccess } from '../actions/albumsAccess'

export class HomeScreen extends Component {

  state= {
    user: this.props.user.id
  }

  static propTypes = {
    getPicturesAccess: PropTypes.func.isRequired,
    getAlbumsAccess: PropTypes.func.isRequired,
  }

  
  componentDidMount() {
    const user = this.state.user
    this.props.getPicturesAccess(user)
    this.props.getAlbumsAccess(user)
  }

  render () {
    return (
      <ScrollView style={styles.container}>
        <Text style={styles.welcome}>Hello,{this.props.user.username} </Text>
        <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between' }}>
        </View>
        <Text style={styles.titleAlbum}> Shared Pictures </Text>
        { this.props.picturesAccess.picturesAccess.length != 0 ? 
        (  
          <Text> {this.props.picturesAccess.picturesAccess.map((obj, index) => (
            <View key={index}>
                  <TouchableHighlight 
                    style={styles.touchable} 
                    onPress={() => 
                      this.props.navigation.navigate('DetailPictureAccessScreen',
                      { 
                        pictureId: obj.id,
                        pictureName: obj.name,
                        pictureLink: obj.link,
                        pictureDate: obj.date,
                        pictureAlbum: obj.albumid,
                      }
                      )
                    }
                  >
                    <View>
                      <Image
                        style={styles.fullImageStyle}
                        source={{
                          uri: obj.link
                        }}
                      />
                    <TextInput style={styles.name} >{ obj.name }</TextInput>
                  </View>
                  </TouchableHighlight>
            </View>
            
            )
          )}
          </Text>
        ) 
        : ( <Text> No pictures are shared </Text> )
        }
        <Text style={styles.titleAlbum}>Shared albums: </Text>
        { this.props.albumsAccess.albumsAccess.length != 0 ? 
        (
          <Text> { this.props.albumsAccess.albumsAccess.map((obj, index) => (
            <View key={index}>
                <TouchableHighlight 
                  style={styles.touchable} 
                  onPress={() => this.props.navigation.navigate('DetailAlbumAccessScreen',
                    {
                      albumid: obj.id,
                      albumName: obj.name,
                    }
                  )}
                >
                  <Image
                    style={styles.fullImageStyle}
                    source={{
                      uri: '/Users/rezigibtissam/Documents/Epitech/PictsManager/front/pictsmanager/assets/album.png',
                    }}
                  />
                </TouchableHighlight>
                <TextInput style={styles.name}>{obj.name}</TextInput>
            </View>
            )
          )} 
          </Text>
          ) : ( <Text> No Albums are shared</Text> )
        }
      </ScrollView>
    )
  }
    
}

const mapStateToProps = (state) => ({
  picturesAccess: state.picturesAccess,
  albumsAccess: state.albumsAccess,
  user: state.auth.user,
})

export default connect(mapStateToProps, { getAlbumsAccess, getPicturesAccess })(HomeScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
  },
  name: {
    fontSize : 18,
    marginLeft: 17,
    color: '#003f5c',
  },
  fullImageStyle:{
    width: 150,
    height: 120,
    marginBottom: 5,
  },
  welcome: {
    marginLeft: 5,
    marginTop: 30,
    fontSize: 40,
  },
  button: {
    width: 160,
    flexDirection: 'row',
    backgroundColor: "#CCCCCC",
    borderRadius: 12,
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 0,
    marginBottom: 20
  },
  titleAlbum: {
    fontSize: 20,
    marginBottom: 10,
    marginTop: 30,
  },
  touchable: {
    width:150, 
    height:120,
  }
})