import React, { Component } from 'react'
import { View, ScrollView, Text, Image, Alert, StyleSheet, Button, TouchableHighlight } from 'react-native'
import { TouchableOpacity, TextInput } from 'react-native-gesture-handler'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { AntDesign } from '@expo/vector-icons'
import FlashMessage from "react-native-flash-message"
import { showMessage } from "react-native-flash-message"

import { getPictures } from '../actions/pictures'
import { getAlbums } from '../actions/albums'
import { addAlbum } from '../actions/albums'

export class ProfileScreen extends Component {
  state= {
    user: this.props.user.id
  }
  static propTypes = {
    getAlbums: PropTypes.func.isRequired,
    getPictures: PropTypes.func.isRequired,
    addAlbum: PropTypes.func.isRequired,
  }

  createPromptAlert = () =>
    Alert.prompt(
      "Enter Name",
      "Enter a name for your album ",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: (albumName) => {
            this.props.addAlbum(this.state.user, albumName);
            showMessage({
              message: "New Album was created",
              description: 'A new album was successfully created',
              type: "success",
            })
          }
        }
      ],
    );

  componentDidMount() {
    const user = this.state.user
    this.props.getAlbums(user)
    this.props.getPictures(user)
  }

  render () {
    return (
      <ScrollView style={styles.container}>
         <FlashMessage ref="Top" />
        <Text style={styles.welcome}>Welcome, {this.props.user.username} </Text>
        <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between' }}>

          <TouchableOpacity 
            style={styles.button}
            title='add a new picture'
            onPress={()=> this.props.navigation.navigate('CameraScreen') }
          >
            <AntDesign name="addfile" size={19} color="black" />
            <Text>add a new picture</Text>
          </TouchableOpacity>

          <TouchableOpacity 
            style={styles.button}xs
            title=' add a new album' 
            onPress={ ()=> this.createPromptAlert() }
          >
            <AntDesign name="addfolder" size={20} color="black" />
            <Text>add a new album</Text>
          </TouchableOpacity>
        </View>

       <Text style={styles.titleAlbum}>My Pictures:</Text>
        <Text> {this.props.pictures.pictures.map((obj, index) => (
          <View key={index}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => 
                this.props.navigation.navigate('DetailPictureScreen',
                  { 
                    pictureId: obj.id,
                    pictureName: obj.name,
                    pictureLink: obj.link,
                    pictureDate: obj.date,
                    pictureAlbum: obj.albumid,
                  }
                )
              }
            >
              <View>
                <Image
                  style={styles.fullImageStyle}
                  source= {{uri: obj.link} || '/Users/rezigibtissam/Documents/Epitech/PictsManager/front/pictsmanager/assets/album.png' }
                />
                <Text style={styles.welcome}> {obj.name} </Text>
              </View>
            </TouchableHighlight>
            
          </View>
          )
        )}
        </Text>
        <Text style={styles.titleAlbum}>My Albums:</Text>
          <Text>{ this.props.albums.albums.map((obj, index) => (
            <View key={index}>
                <TouchableHighlight 
                  style={styles.touchable}
                  onPress={() => 
                    this.props.navigation.navigate('DetailAlbumScreen', 
                    {
                      albumid: obj.id,
                      albumName: obj.name,
                    }
                  )}>
                <Image
                  style={styles.fullImageStyle}
                  source={{
                    uri: '/Users/rezigibtissam/Documents/Epitech/PictsManager/front/pictsmanager/assets/album.png',
                  }}
                />
                </TouchableHighlight>
                <TextInput style={styles.name}> {obj.name}</TextInput>
            </View>
            )
          )} </Text>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => ({
  pictures: state.pictures,
  albums: state.albums,
  user: state.auth.user,
})


export default connect((mapStateToProps), { getAlbums, getPictures, addAlbum })(ProfileScreen)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
  },
  name: {
    fontSize : 18,
    marginLeft: 10,
    color: '#003f5c',
  },
  fullImageStyle:{
    width: 150,
    height: 120,
    borderRadius: 10,
    marginBottom: 5,
  },
  welcome: {
    marginLeft: 5,
    marginTop: 20,
    marginBottom: 20,
    fontSize: 20,
  },
  button: {
    width: 160,
    flexDirection: 'row',
    backgroundColor: "#CCCCCC",
    borderRadius: 12,
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 0,
    marginBottom: 20
  },
  titleAlbum: {
    fontSize: 18,
    marginBottom: 10,
  },
  touchable: {
    width:150, 
    height:120
  }
})