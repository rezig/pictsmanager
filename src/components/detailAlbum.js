import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableHighlight, TouchableOpacity, Alert, Image, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { showMessage } from "react-native-flash-message"

import { getAlbumPictures } from '../actions/pictures'
import { deleteAlbum } from '../actions/albums'
import { addAlbumAccess, deleteAlbumAccess } from '../actions/albumsAccess'

export class DetailAlbumeScreen extends Component {
  
  static propTypes = {
    getAlbumPictures: PropTypes.func.isRequired,
    deleteAlbum: PropTypes.func.isRequired,
    addAlbumAccess: PropTypes.func.isRequired,
    deleteAlbumAccess: PropTypes.func.isRequired,
  }

  createTwoButtonAlert = (albumId) =>
    Alert.alert(
      "Are You Sure",
      "Press yes to delete the album",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", 
        onPress: () => {
          this.props.deleteAlbum(albumId)
          this.props.navigation.goBack()
        }
        }
      ],
      { cancelable: false }
    )

    createPromptAlert = (albumId) =>
      Alert.prompt(
        "Enter user Name",
        "Enter the name of the user that you want to share him this album ",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          {
            text: "OK",
            onPress: (userName) => {
              this.props.addAlbumAccess(userName, albumId);
              showMessage({
                message: "User Access Added",
                description: "A new access user was added",
                type: "success",
              })
            }
          }
        ],
      )

    createAccessdelete = (albumid) =>
      Alert.prompt(
        "Enter user Name",
        "Enter the Name of the user that you want to delete his access to this album ",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          {
            text: "OK",
            onPress: (user) => {
              this.props.deleteAlbumAccess(albumid, user);
              showMessage({
                message: "User Access Deleted",
                description: "The access user was deleted",
                type: "success",
              })
            }
          }
        ],
      )
  
  componentDidMount() {
    this.props.getAlbumPictures(this.props.route.params.albumid)
  }

  render() {
    const { albumid, albumName } = this.props.route.params

    return (
      <ScrollView style={styles.container} >
        <Text style={styles.welcome} >Album Name : {albumName}</Text>
        { this.props.pictures.albumpictures.length != 0 ? (
          <Text> {this.props.pictures.albumpictures.map((obj, index) => 
            (
              <View key={index}>
                <View>
                  <TouchableHighlight
                    style={styles.touchable}
                    onPress={() => 
                      this.props.navigation.navigate('DetailPictureScreen',
                        { 
                          pictureId: obj.id,
                          pictureName: obj.name,
                          pictureLink: obj.link,
                          pictureDate: obj.date,
                          pictureAlbum: obj.albumid,
                        }
                      )
                    }
                  >
                    <Image
                      style={styles.fullImageStyle}
                      source={{
                        uri: obj.link
                      }}
                    />
                  </TouchableHighlight>
                </View>
              </View>
            )
          )}
          </Text>
        ) : ( <Text style={styles.titleAlbum} >This Album is empty</Text> )
        }
        <View style={styles.myButton}>
          <TouchableOpacity
            title='Add Access to Picture'
            onPress={() => this.createPromptAlert(albumid) }
            style={styles.buttonSave}
          >
            <Text style={styles.text}>Add access</Text>
          </TouchableOpacity>

          <TouchableOpacity
            title='Delete this picture'
            onPress={() => this.createTwoButtonAlert(albumid) }
            style={styles.buttonDelete}
          >
            <Text style={styles.text}>Delete Album</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          title='Delete Access Picture'
          onPress={() => this.createAccessdelete(albumid) }
          style={styles.btn}
        >
          <Text style={styles.text}>Delete Access</Text>
        </TouchableOpacity>
        
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => ({
  pictures: state.pictures,
})


export default connect((mapStateToProps), { getAlbumPictures, deleteAlbum, addAlbumAccess, deleteAlbumAccess })(DetailAlbumeScreen)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
    marginTop: 30,
  },
  btn: {
    width: "50%",
    backgroundColor: "#CCCCCC",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 90,
  },
  fullImageStyle:{
    width: 150,
    height: 120,
    marginBottom: 20,
  },
  welcome: {
    marginLeft: 5,
    marginTop: 20,
    marginBottom: 20,
    fontSize: 20,
  },
  button: {
    width: 160,
    flexDirection: 'row',
    backgroundColor: "#CCCCCC",
    borderRadius: 12,
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 0,
    marginBottom: 20
  },
  titleAlbum: {
    fontSize: 18,
    marginBottom: 10,
    marginLeft: 23,
  },
  touchable: {
    width:150, 
    height:120
  },
  button:{
    width: "50%",
    backgroundColor: "#fb5b5a",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10,
    marginLeft: 92,
  },
  text:{
    marginLeft: 5,
    fontSize: 22,
    color: 'white'
  },
  buttonDelete:{
    width: "45%",
    backgroundColor: "#fb5b5a",
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10,
  },
  buttonSave:{
    width: "40%",
    backgroundColor: "green",
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10,
  },
  myButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
})