import React, { useState, useEffect, useRef } from 'react'
import { Text, View, TouchableOpacity, StyleSheet, Modal, Image, SafeAreaView } from 'react-native'
import { Camera } from 'expo-camera'
import { Ionicons, FontAwesome } from '@expo/vector-icons'

export default function CameraScreen({ navigation }) {

  const camRef = useRef(null)
  const [hasPermission, setHasPermission] = useState(null)
  const [type, setType] = useState(Camera.Constants.Type.back)
  const [capturedPicture, setcapturedPicture] = useState(null)
  const [open, setOpen] = useState(false)

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestPermissionsAsync()
      setHasPermission(status === 'granted')
    })()
  }, [])

  if (hasPermission === null) {
    return <View />
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>
  }

  async function takePicture() {
    if (camRef) {
      const data = await camRef.current.takePictureAsync()
      setcapturedPicture(data.uri)
      setOpen(true)
      navigation.navigate('SavePictureScreen', {pictureUri: data.uri})
    }
  }

  return (
    <View style={styles.container}>
      <Camera 
        style={{ flex: 1 }} 
        type={type}
        ref={camRef}
        >
        <View
          style={{
            flex: 1,
            backgroundColor: 'transparent',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            onPress={() => {
              setType(
                type === Camera.Constants.Type.back
                  ? Camera.Constants.Type.front
                  : Camera.Constants.Type.back
              )
            }}>
              <Text style={styles.flip} >Flip</Text>
          </TouchableOpacity>
        </View>
      </Camera>

      <TouchableOpacity style={styles.button} onPress={takePicture} >
        <Ionicons 
          style={styles.cheese}
          name="md-camera"
          color="white"
          size={40}
        />
      </TouchableOpacity>
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fb5b5a',
    margin: 20,
    borderRadius: 5,
    height: 50,
  },
  flip: {
    marginLeft: 22,
    fontSize: 23,
    alignSelf: 'center',
    color: 'white',
  },
  cheese: {
    alignSelf: 'center',
  }
})