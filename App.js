import * as React from 'react';
import Routing from './src/Routing'
import {Provider} from 'react-redux'
import {store} from './src/store'
import FlashMessage from "react-native-flash-message"

export default function App() {
  return (
    <Provider store={store} >
      <Routing />
      <FlashMessage position="top" />
    </Provider>
  )
}
